import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.Test;

public class EtudiantService {

    public List<Etudiant> etudiantList=new ArrayList<Etudiant>();

    public void ajoutEtudiant(Etudiant etudiant){

        this.etudiantList.add(etudiant);
    }

    public void SupprimerEtudiant(Etudiant etudiant){

        this.etudiantList.remove(etudiant);
    }

    public void ModifierEtudiant(Etudiant etudiant1,Etudiant etudiant2){

    	this.etudiantList.remove(etudiant1);

        etudiantList.add(etudiant2);

    }

    public List<Etudiant> TousLesEtudiants()
    {
        return etudiantList;
    }

}
