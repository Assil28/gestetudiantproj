import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;


public class EtudiantServiceTest {

    Etudiant etudiant = new Etudiant(1L,123,"dekhil","assil","28-06-2000","M1");
    
   
    private EtudiantService etudiantService=new EtudiantService();
    List<Etudiant> etudiants=new ArrayList<>();


    @Test
    public void testAjoutEtudiant() {

        etudiantService.ajoutEtudiant(etudiant);

        
        assertTrue(etudiantService.etudiantList.contains(etudiant));
    }

    @Test
    public void testSupprimerEtudiant() {

        etudiantService.ajoutEtudiant(etudiant);

        etudiantService.SupprimerEtudiant(etudiant);

        List<Etudiant> etudiants = etudiantService.TousLesEtudiants();
        assertFalse(etudiants.contains(etudiant));
    }

    @Test
    public void testModifierEtudiant() {
    	
        etudiantService.ajoutEtudiant(etudiant);
        

        Etudiant etudiantModifie = new Etudiant(1L,123,"assil","assil","28-06-2000","M1");
        etudiantService.ModifierEtudiant(etudiant, etudiantModifie);

       
        assertFalse(etudiantService.etudiantList.contains(etudiant)); // Vérifier que l'étudiant original n'est plus dans la liste
        assertTrue(etudiantService.etudiantList.contains(etudiantModifie)); // Vérifier que l'étudiant modifié est dans la liste
    }
    
    @Test
    public void TestLesEtudiant() {
    	
    	assertTrue(this.etudiants.size()==0);
    	 etudiantService.ajoutEtudiant(etudiant);
        this.etudiants=this.etudiantService.TousLesEtudiants();
        assertTrue(this.etudiants.size()==1);
    

       
       
    }
 
}
