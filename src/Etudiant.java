
public class Etudiant {

    private Long id;
    private Integer numinscription;
    private String nom;
    private String prenom;
    private String datenaissance;
    private String niveau;
    
    
	public Etudiant(Long id, Integer numinscription, String nom, String prenom, String datenaissance, String niveau) {
		super();
		this.id = id;
		this.numinscription = numinscription;
		this.nom = nom;
		this.prenom = prenom;
		this.datenaissance = datenaissance;
		this.niveau = niveau;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Integer getNuminscription() {
		return numinscription;
	}


	public void setNuminscription(Integer numinscription) {
		this.numinscription = numinscription;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public String getDatenaissance() {
		return datenaissance;
	}


	public void setDatenaissance(String datenaissance) {
		this.datenaissance = datenaissance;
	}


	public String getNiveau() {
		return niveau;
	}


	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}
    
    
    

}
